#include <stdio.h>
#include <stdlib.h>

#include "bmp.h"
#include "image.h"
#include "transformation.h"

int main(int argc, char* argv[]) {
    if (argc != 4) {
        printf("Usage: ./image-transformer <source-image> <transformed-image> <angle>\n");
        return 1;
    }

    char* source_image_path = argv[1];
    char* transformed_image_path = argv[2];
    int angle = atoi(argv[3]);

    if (angle % 90 != 0) {
        printf("Invalid angle. Supported angles are 0, 90, -90, 180, -180, 270, -270.\n");
        return 1;
    }

    struct image source;
    if (from_bmp(fopen(source_image_path, "rb"), &source) != READ_OK) {
        printf("Error reading the source image.\n");
        return 1;
    }

    struct image transformed = rotate_image(&source, angle);

    if (to_bmp(fopen(transformed_image_path, "wb"), &transformed) != WRITE_OK) {
        printf("Error writing the transformed image.\n");
        return 1;
    }

    destroy_image(&source);
    destroy_image(&transformed);

    return 0;
}
