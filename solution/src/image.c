#include "image.h"
#include <stdint.h>
#include <stdlib.h>


struct image create_image(uint64_t width, uint64_t height) {
    struct image img;
    img.width = width;
    img.height = height;
    img.data = malloc(sizeof(struct pixel) * width * height);

    if (!img.data) {
        img.width = img.height = 0;
    }

    return img;
}

void destroy_image(struct image* img) {
    if (img && img->data) {
        free(img->data);
        img->data = NULL;
        img->width = img->height = 0;
    }
}
