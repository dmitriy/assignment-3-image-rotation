#include "transformation.h"
#include "image.h"
#include <stdint.h>

#include "transformation.h"
struct image rotate_image(struct image const* source, int angle) {
    if (angle == 0) {
        struct image rotated = create_image(source->width, source->height);
        for (uint64_t y = 0; y < source->height; y++) {
            for (uint64_t x = 0; x < source->width; x++) {
                rotated.data[y * rotated.width + x] = source->data[y * source->width + x];
            }
        }

        return rotated;
    }

    if (angle == 90 || angle == -270) {
        struct image rotated = create_image(source->height, source->width);

        for (uint64_t y = 0; y < source->height; y++) {
            for (uint64_t x = 0; x < source->width; x++) {
                rotated.data[(rotated.height - x - 1) * rotated.width + y] = source->data[y * source->width + x];
            }
        }

        return rotated;
    }

    if (angle == -90 || angle == 270) {
        struct image rotated = create_image(source->height, source->width);

        for (uint64_t y = 0; y < source->height; y++) {
            for (uint64_t x = 0; x < source->width; x++) {
                rotated.data[x * rotated.width + (rotated.width - y - 1)] = source->data[y * source->width + x];
            }
        }

        return rotated;
    }

    if (angle == 180 || angle == -180) {
        struct image rotated = create_image(source->width, source->height);

        for (uint64_t y = 0; y < source->height; y++) {
            for (uint64_t x = 0; x < source->width; x++) {
                rotated.data[(rotated.height - y - 1) * rotated.width + (rotated.width - x - 1)] = source->data[y * source->width + x];
            }
        }

        return rotated;
    }

    return *source;
}
