#include <stdint.h>

int is_power_of_two(uint64_t x) {
    return (x != 0) && ((x & (x - 1)) == 0);
}
