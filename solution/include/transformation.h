#ifndef TRANSFORMATION_H
#define TRANSFORMATION_H

#include "image.h"

struct image rotate_image(struct image const* source, int angle);

#endif
