#include "bmp.h"
#include "image.h"
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#define BMP_SIGNATURE 0x4D42
#define BMP_BIT_COUNT 24
#define BMP_COMPRESSION 0
#define BMP_BIT_SIZE 40

enum read_status from_bmp(FILE* in, struct image* img) {
    if (!in || !img) {
        return READ_INVALID_HEADER;
    }

    struct bmp_header header;
    if (fread(&header, sizeof(header), 1, in) != 1) {
        return READ_INVALID_HEADER;
    }

    if (header.bfType != BMP_SIGNATURE || header.biBitCount != BMP_BIT_COUNT || header.biCompression != BMP_COMPRESSION) {
        return READ_INVALID_HEADER;
    }

    *img = create_image(header.biWidth, header.biHeight);

    if (!img->data) {
        return READ_INVALID_BITS;
    }

    int padding = (4 - (int)(img->width * sizeof(struct pixel)) % 4) % 4;

    for (uint64_t y = 0; y < img->height; y++) {
        for (uint64_t x = 0; x < img->width; x++) {
            if (fread(&img->data[y * img->width + x], sizeof(struct pixel), 1, in) != 1) {
                destroy_image(img);
                return READ_INVALID_BITS;
            }
        }
        if (fseek(in, padding, SEEK_CUR) != 0) {
            destroy_image(img);
            return READ_INVALID_BITS;
        }
    }

    return READ_OK;
}

enum write_status to_bmp(FILE* out, struct image const* img) {
    if (!out || !img || !img->data) {
        return WRITE_ERROR;
    }

    struct bmp_header header;
    memset(&header, 0, sizeof(header));
    header.bfType = BMP_SIGNATURE;
    header.bfileSize = sizeof(header) + (img->width * sizeof(struct pixel) + ((4 - (img->width * sizeof(struct pixel)) % 4) % 4)) * img->height;
    header.bOffBits = sizeof(header);
    header.biSize = BMP_BIT_SIZE;
    header.biWidth = img->width;
    header.biHeight = img->height;
    header.biPlanes = 1;
    header.biBitCount = BMP_BIT_COUNT;

    if (fwrite(&header, sizeof(header), 1, out) != 1) {
        return WRITE_ERROR;
    }

    int padding = (4 - (int)(img->width * sizeof(struct pixel)) % 4) % 4;

    for (uint64_t y = 0; y < img->height; y++) {
        for (uint64_t x = 0; x < img->width; x++) {
            if (fwrite(&img->data[y * img->width + x], sizeof(struct pixel), 1, out) != 1) {
                return WRITE_ERROR;
            }
        }
        uint8_t pad = 0;
        for (int i = 0; i < padding; i++) {
            if (fwrite(&pad, 1, 1, out) != 1) {
                return WRITE_ERROR;
            }
        }
    }

    return WRITE_OK;
}
