#ifndef UTILS_H
#define UTILS_H

#include <stdint.h>

int is_power_of_two(uint64_t x);

#endif
